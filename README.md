# termux-pkg-test

LICENSE: MPL 2.0

This code is designed to work only on Termux (for Android).

DO NOT USE THIS CODE UNLESS YOU READ AND FULLY UNDERSTAND IT AND THE LICENSE.

Clone this git repo.

Navigate to src/

To build the package, use build-package.sh

To build the apt repo, use build-apt-repo.sh

To install the repo, use install-repo.sh

To install the package, use pkg install termux-pkg-test.sh

Dependencies are not yet listed. Just pay attention to any errors and they will indicate what's missing. I'll fix it later.
